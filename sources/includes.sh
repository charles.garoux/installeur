#!/bin/bash

# Colors
CR="\033[1;31m"
CG="\033[1;32m"
CY="\033[1;33m"
CB="\033[1;34m"
CM="\033[1;35m"
CC="\033[1;36m"
CE="\033[0m"

# Clear Line
CL="\033[2K"

# VARIABLES
OUTPUT_LOG_FILE="/var/log/WebServerInstaller-output.log"
LOG_FILE="/var/log/WebServerInstaller.log"
DOING=""
PHPVER="php7.2"
ADMINERVER="adminer-4.7.1"

# FUNCTION: Informs the user about the current operation
function inform
{
	echo -ne " \r ${CL}${CC}[..]${CE} - ${DOING}"
}

function success
{
	echo -e " \r ${CL}${CG}[OK]${CE} - ${DOING}"
	log "OK" "${DOING}"
}

function failure
{
	echo -e " \r ${CL}${CR}[FAILED]${CE} - ${DOING}"
	log "FAIL" "${DOING}"

	echo -e ""
	echo -e " ${CL}${CR}---| LATEST LINES |---"
	tail -15 ${OUTPUT_LOG_FILE}
	echo -e ""
	echo -e " ${CE}For more informations, check ${CY}${OUTPUT_LOG_FILE}${CE}"
}

function log
{
	echo -e "$(date '+%d/%m/%Y %H:%M:%S') - [$1] - ${2}" >> ${LOG_FILE}
}

# FUNCTION: Checks the error code
function check
{
	if [[ $? -eq 0 ]] && [[ $@ -eq 0 ]] || [[ $1 -eq 0 ]]
	then
		success
	else
		failure
		echo -e " "
		exit 1
	fi
}

# FUNCTION: Waits for the user's input
function waitUserInput
{
	read -p " Appuyez sur entrer pour continuer... "
	echo -e " "
}

SOURCES_FILES="packages apache2 php mariadb ssh adminer composer application"
for file in $SOURCES_FILES; do
	DOING="Chargement du fichier source ${file}.sh [source]"
	inform
	source ${DIR}/sources/${file}.sh &>>${LOG_FILE}
	check
done
