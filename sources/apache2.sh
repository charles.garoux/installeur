#! /bin/bash

function installer_apache2
{
	clear
	echo -e " "
	echo -e " ${CC}INSTALLATION DE APACHE2${CE} "
	echo -e " "

	DOING="Installation de apache2 [apt install apache2]"
	inform
	sudo apt-get install --assume-yes apache2 &>> ${OUTPUT_LOG_FILE}
	check

	DOING="Changement des permissions [chmod 755]"
	inform
	sudo chmod 755 -R /var/www &>> ${OUTPUT_LOG_FILE}
	check
}


# $1 = nom de la configuration (certbot, laravel, laravel_certbot)
# $2 = "nom-de-domaine.xxx" est le nom de domaine pour le site
# $NOM_PROJET est le nom du projet Laravel, il est indiqué dans sources/laravel.sh
function configuration_apache2
{
	clear
	echo -e " "
	echo -e " ${CC}CONFIGURATION DE APACHE2${CE} "
	echo -e " configuration: ${1}"

	if [[ -f /etc/apache2/sites-available/website.conf ]]
	then
		CONFIRM=false
		while [[ $CONFIRMM != "O" && $CONFIRM != "o" && $CONFIRM != "N" && $CONFIRM != "n" ]]; do
			echo -e " "
			read -p " website.conf existe déjà. Voulez-vous l'ecraser ? [O/N]: " CONFIRM
		done
	else
		CONFIRM="O"
	fi

	if [[ $CONFIRM == "O" ]] || [[ $CONFIRM == "o" ]]
	then
		DOING="Ajout de la configuration apache2 pour Laravel en http [sudo cp]"
		inform
		sudo cp -f "${DIR}/config/apache2_laravel.conf" "/etc/apache2/sites-available/website.conf"  &>> ${OUTPUT_LOG_FILE}
		check
	fi


	echo -e " "
	echo -e " ${CR}A modifier manuellement dans /etc/apache2/apache2.conf ${CE}"
	echo -e "________________________________________________________"
	echo -e " <Directory /var/www/>"
	echo -e "	Options Indexes FollowSymLinks"
	echo -e "	AllowOverride ${CR}None${CE}"
	echo -e "	Require all granted"
	echo -e "</Directory>"
	echo -e "________________________________________________________"
	echo -e "<Directory /var/www/>"
	echo -e "	Options Indexes FollowSymLinks"
	echo -e "	AllowOverride ${CG}All${CE}"
	echo -e "	Require all granted"
	echo -e "</Directory>"
	echo -e "________________________________________________________"
	echo -e " "

	waitUserInput

	DOING="Activation du module rewrite [sudo a2enmod]"
	inform
	sudo a2enmod rewrite &>> ${OUTPUT_LOG_FILE}
	check

	cd /var/www/${NOM_PROJET}
	DOING="Nettoyage du cache [sudo php artisan]"
	inform
	sudo php artisan cache:clear &>> ${OUTPUT_LOG_FILE}
	check

	DOING="Creation d'un lien symbolique [sudo php artisan]"
	inform
	sudo php artisan storage:link &>> ${OUTPUT_LOG_FILE}
	check

	DOING="Changement de prorietaire du projet a www-data [sudo chown]"
	inform
	sudo chown www-data . -R
	check

	DOING="Désactivation du site par défaut [a2dissite]"
	inform
	sudo a2dissite 000-default &>> ${OUTPUT_LOG_FILE}
	check

	DOING="Activation de la configuration [a2ensite]"
	inform
	sudo a2ensite website &>> ${OUTPUT_LOG_FILE}
	check

	DOING="Activation des modules 1/3 [a2enmod ssl]"
	inform
	sudo a2enmod ssl &>> ${OUTPUT_LOG_FILE}
	check

	DOING="Activation des modules 2/3 [a2enmod headers]"
	inform
	sudo a2enmod headers  &>> ${OUTPUT_LOG_FILE}
	check

	DOING="Activation des modules 3/3 [a2enmod rewrite]"
	inform
	sudo a2enmod rewrite  &>> ${OUTPUT_LOG_FILE}
	check

	DOING="Redémarrage d'APACHE [service apache2 restart]"
	inform
	sudo service apache2 restart  &>> ${OUTPUT_LOG_FILE}
	check
}
