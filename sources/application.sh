#! /bin/bash

function installer_application
{
	clear
	echo -e " "
	echo -e " ${CC}INSTALLATION DE L'APPLICATION${CE}"
	echo -e " "

	cd /var/www
	DOING="Clone de l'application depuis le dépôt git [sudo git clone]"
	inform
	sudo git clone https://gitlab.com/charles.garoux/application-mdm-remix.git &>> ${OUTPUT_LOG_FILE}
	check

	cd /var/www/html
	DOING="Creation d'un lien symbolique [sudo ln -s]"
	inform
	sudo ln -s ../application-mdm-remix/public application_mdm &>> ${OUTPUT_LOG_FILE}
	check

	cd /var/www/application-mdm-remix
	DOING="Installation avec composer [sudo composer install]"
	inform
	sudo composer install &>> ${OUTPUT_LOG_FILE}
	check

	DOING="Creation du .env [sudo cp]"
	inform
	sudo cp .env.exemple .env &>> ${OUTPUT_LOG_FILE}
	check

	DOING="Generation d'une cle [sudo php artisan]"
	inform
	sudo php artisan key:generate &>> ${OUTPUT_LOG_FILE}
	check


	DOING="Changement de propriete du repertoire de l'application a www-data"
	inform
	sudo chown www-data . -R &>> ${OUTPUT_LOG_FILE}
	check

	echo -e "${CR}Quand l'installation sera terminé il faudra compléter le .env et remplire la base de données SQL.${CE}"
	waitUserInput

}

