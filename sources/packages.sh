#! /bin/bash
function mise_a_jour
{
	clear
	echo -e " "
	echo -e " ${CC}GESTION DES PAQUETS${CE} "
	echo -e " "
	echo -e " Le script va maintenant mettre a jours votre système et "
	echo -e " installer quelques dépendances qui seront nécessaires prochainement"
	echo -e " "

	APT_COMMANDS="update full-upgrade autoremove autoclean"
	for apt_command in $APT_COMMANDS; do
		DOING="Mise a jour du systeme [apt ${apt_command}]"
		inform
		sudo apt ${apt_command} -y &>> ${OUTPUT_LOG_FILE}
		check
	done

	APT_PACKAGES="apt-transport-https lsb-release ca-certificates"
	for package in $APT_PACKAGES; do
		DOING="Installation des paquets [apt install ${package}]"
		inform
		sudo apt install -y ${package} &>> ${OUTPUT_LOG_FILE}
		check
	done
}
