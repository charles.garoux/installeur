#! /bin/bash

function installer_mariadb
{
	clear
	echo -e " "
	echo -e " ${CC}INSTALLATION MARIADB${CE} "
	echo -e " "
	echo -e " Le script va maintenant installer un serveur MariaDB puis"
	echo -e " faire une installation securise"
	echo -e " "

	DOING="Installation du paquet mariadb-server [sudo apt-get]"
	inform
	sudo apt-get --assume-yes install mariadb-server &>> ${OUTPUT_LOG_FILE} 
	check

	DOING="Installation securise du serveur mariadb [mysql-secure-installation]"
	inform
	sudo mysql_secure_installation
	check
}
