#! /bin/bash

function installer_php
{

	clear
	echo -e " "
	echo -e " ${CC}INSTALLATION DE ${PHPVER^^}${CE} "
	echo -e " Le script va maintenant installer ${PHPVER} puis mettre un fichier php"
	echo -e " avec phpinfo() pour verifier avec un navigateur web si ${PHPVER} est bien installe"	
	echo -e " "

	DOING="Téléchargement de la clé APT SURY [wget]"
	inform
	sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg &>>${OUTPUT_LOG_FILE}
	check

	DOING="Ajout du dépot SURY [sh]"
	inform
	sudo sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list' &>>${OUTPUT_LOG_FILE}
	check

	DOING="Mise a jour du système[apt-get update]"
	inform
	sudo apt-get update &>>${OUTPUT_LOG_FILE}
	check

	APT_PACKAGES="${PHPVER} ${PHPVER}-cli ${PHPVER}-common ${PHPVER}-fpm ${PHPVER}-curl ${PHPVER}-mbstring ${PHPVER}-gd ${PHPVER}-intl ${PHPVER}-xml ${PHPVER}-mysql ${PHPVER}-zip ${PHPVER}-xdebug"
	for package in $APT_PACKAGES; do
		DOING="Installation des paquets [apt install ${package}]"
		inform
		sudo apt-get install --assume-yes ${package} &>>${OUTPUT_LOG_FILE}
		check
	done

	DOING="Ajout phpinfo.php dans /var/www/html/ [cp]"
	inform
	sudo cp ${DIR}/config/phpinfo.php /var/www/html/ &>> ${OUTPUT_LOG_FILE}
	check
}
