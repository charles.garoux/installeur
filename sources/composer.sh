#!/bin/bash

function installer_composer
{
	cd ${DIR}
	DOING="Telechargement de l'installeur de composer [sudo wget]"
	inform
	sudo wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer &>> ${OUTPUT_LOG_FILE}
	check
	
	DOING="Execution php de l'installeur [sudo php]"
	inform
	sudo php ${DIR}/installer &>> ${OUTPUT_LOG_FILE}
	check

	DOING="Suppression de l'installeur [sudo rm]"
	inform
	sudo rm ${DIR}/installer &>> ${OUTPUT_LOG_FILE}
	check

	DOING="Deplacer composer.phar dans /usr/local/bin/composer [sudo mv]"
	inform
	sudo mv ${DIR}/composer.phar /usr/local/bin/composer &>> ${OUTPUT_LOG_FILE}
	check
}
