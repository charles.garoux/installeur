#! /bin/bash

function installer_adminer
{
	VERSION=$(echo $ADMINERVER | cut -d "-" -f 2)

	DIR=/var/www/html
	DOING="Telechargement de ${ADMINERVER} [wget]"
	inform
	sudo wget https://www.adminer.org/static/download/${VERSION}/${ADMINERVER}.php -O ${DIR}/adminer.php
	check
}
