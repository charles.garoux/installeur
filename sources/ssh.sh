#! /bin/bash

function ajouter_sshkey
{

	REGEX_SSHKEY="^(ssh-([a-z]{3}) [A-Za-z0-9/+]{372} [A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6})$" 
	SSHKEY=""

	while [ -z "$SSH_USER" ] || [ "$SSH_USER" != "$USER_FIND" ]
	do
		echo -e "\nSi vous souhaiter ajouter la cle ssh a l'utilisateur qui execute le script, ne rien entrer."
		read -p "Entrer l'utilisateur existant a qui ajouter la cle ssh:" SSH_USER

		if [ -z $SSH_USER ]
		then
			SSH_USER=$USER
		fi

		#echo "SSH_USER=${SSH_USER}" #debug
		USER_INFO=$(cat /etc/passwd | grep $SSH_USER | head -n 1)
		#echo "USER_INFO=${USER_INFO}" #debug
		USER_FIND=$(echo $USER_INFO | cut -d ":" -f 1)
		#echo "USER_FIND=${USER_FIND}" #debug
	done
	USER_HOME=$(echo ${USER_INFO} | cut -d ":" -f 6)
	#echo "USER_HOME=${USER_HOME}" #debug

	while [[ ! $SSHKEY =~ $REGEX_SSHKEY ]]
	do
		read -p "Inserez la cle ssh a la suite dans le terminale:" SSHKEY
		if [[ ! $SSHKEY =~ $REGEX_SSHKEY ]]
		then
			echo "Cle invalide, veuillez entrer une cle ssh valide."
		fi
	done


	if sudo bash -c "[ ! -d "${USER_HOME}/.ssh" ]" #si n'existe pas
	then
		#echo "${USER_HOME}/.ssh existe pas" #debug
		if [ -w ${USER_HOME} ] #checker les droit, est qu'on peu creer le dir a l'endroit voulu
		then
			#echo "a les droit" #debug

			DOING="Creation du repertoire ${USER_HOME}.ssh [mkdir]"
			inform
			mkdir ${USER_HOME}/.ssh &>> ${OUTPUT_LOG_FILE}
			check
			DOING="Changement de propietaire de ${USER_HOME}.ssh [chown]"
			inform
			chown ${SSH_USER} ${USER_HOME}/.ssh &>> ${OUTPUT_LOG_FILE}
			check
			DOING="Ajout de la cle dans ${USER_HOME}.ssh/authorized_keys [echo]"
			inform
			echo ${SSHKEY} >> ${USER_HOME}/.ssh/authorized_keys
			check
			DOING="Changement de propietaire de ${USER_HOME}.ssh/authorized_keys ${USER}->${SSH_USER} [chown]"
			inform
			chown ${SSH_USER} ${USER_HOME}/.ssh/authorized_keys &>> ${OUTPUT_LOG_FILE}
			check
		else
			#echo "n'a pas les droit" #debug

			DOING="Creation du repertoire ${USER_HOME}.ssh [mkdir]"
			inform
			sudo mkdir ${USER_HOME}/.ssh &>> ${OUTPUT_LOG_FILE}
			check
			DOING="Changement de propietaire de ${USER_HOME}.ssh [chown]"
			inform
			sudo chown ${SSH_USER} ${USER_HOME}/.ssh &>> ${OUTPUT_LOG_FILE}
			check
			DOING="Ajout de la cle dans ${USER_HOME}.ssh/authorized_keys [echo]"
			inform
			sudo sh -c "echo ${SSHKEY} >> ${USER_HOME}/.ssh/authorized_keys" #pas de "&>> ${OUTPUT_LOG_FILE}" pour eviter que la cle finise dans le fichier de log
			check
			DOING="Changement de propietaire de ${USER_HOME}.ssh/authorized_keys ${USER}->${SSH_USER} [chown]"
			inform
			sudo chown ${SSH_USER} ${USER_HOME}/.ssh/authorized_keys &>> ${OUTPUT_LOG_FILE}
			check
		fi
	else
		#echo "${USER_HOME}/.ssh existe" #debug
		
		if [ -w ${USER_HOME} ] #checker les droit, est qu'on peu creer le dir a l'endroit voulu
		then

			DOING="Ajout de la cle dans ${USER_HOME}.ssh/authorized_keys [echo]"
			inform
			echo ${SSHKEY} >> ${USER_HOME}/.ssh/authorized_keys &>> ${OUTPUT_LOG_FILE}
			check
			DOING="Changement de propietaire de ${USER_HOME}.ssh/authorized_keys ${USER}->${SSH_USER} [chown]"
			inform
			chown ${SSH_USER} ${USER_HOME}/.ssh/authorized_keys &>> ${OUTPUT_LOG_FILE}
			check


			#echo ${SSHKEY} >> ${USER_HOME}/.ssh/authorized_keys
		else

			DOING="Ajout de la cle dans ${USER_HOME}.ssh/authorized_keys [echo]"
			inform
			sudo sh -c "echo ${SSHKEY} >> ${USER_HOME}/.ssh/authorized_keys"
			check
			DOING="Changement de propietaire de ${USER_HOME}.ssh/authorized_keys ${USER}->${SSH_USER} [chown]"
			inform
			sudo chown ${SSH_USER} ${USER_HOME}/.ssh/authorized_keys &>> ${OUTPUT_LOG_FILE}
			check

			#sudo sh -c "echo ${SSHKEY} >> ${USER_HOME}/.ssh/authorized_keys"
		fi
	fi
	#sudo ls -la ${USER_HOME} #debug
	#sudo ls -la ${USER_HOME}/.ssh #debug
	#sudo cat ${USER_HOME}/.ssh/authorized_keys #debug

}

function installer_ssh
{
	clear
	echo -e " "
	echo -e " ${CC}INSTALLATION SSH${CE} "
	echo -e " "
	echo -e " Le script va maintenant installer un serveur openssh puis"
	echo -e " recuperer un utilisateur et lui ajouter une cle ssh pour y acceder"
	echo -e " a l'utilisateur par ssh de maniere securise"
	echo -e " "


	DOING="Installation du paquet [apt-get install openssh-server]"
	inform
	sudo apt-get --assume-yes install openssh-server &>> ${OUTPUT_LOG_FILE}
	check

	REPONSE=""
	while [ ! "$REPONSE" == "O" ] && [ ! "$REPONSE" == "o" ] && [ ! "$REPONSE" == "n" ]
	do
		read -p "Souhaitez vous ajouter une cle ssh a une utilisateur ? [O/n] " REPONSE
		echo $REPONSE
	done

	if [ "$REPONSE" == "O" ] || [ "$REPONSE" == "o" ]
	then
		ajouter_sshkey
	fi
}
