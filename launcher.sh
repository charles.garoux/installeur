#! /bin/bash

function script_header
{
	clear
	echo -e " "
	echo -e " ${CC}Web Serveur Intaller${CE} "
	echo -e " "
	echo -e " Le script a pour bute d'installer une serveur web"
	echo -e " Laravel avec une base de donnees"
	echo -e " "
}

clear
echo -e " "
echo -e " \033[1;33mPres requis\033[0m "
echo -e " "
echo -e " Pour fonctionner correctement :"
echo -e " - le script a besoin de la commande \"sudo\""
echo -e " - le script a besoin que l'utilisateur qui execute le script possede des droits \"sudo\""
echo -e " "
echo -e " \033[1;31mSi $USER ne possede pas ces droits devez quitter ce script sinon"
echo -e " le script ne pourra pas fonctionner correctement.\033[0m"
echo -e " "

read -p " Appuyer sur entrer pour continuer"
echo -e " "

echo -e "Verification des droits sudo du $USER:"
sudo -v
if [ $? -eq 1 ]
then
	echo -e " "
	echo -e "\033[1;31mVous devez avoir les droits sudo."
	exit
else
	echo -e " "
	echo -e "\033[1;32mVous avez les droits sudo."
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/sources/includes.sh

script_header

DOING="Creation des fichiers de logs avec des droits [sudo touch + sudo chmod 666]"
inform
sudo touch ${OUTPUT_LOG_FILE}
EXIT_STATUS=$[$EXIT_STATUS+$?]
sudo chmod 666 ${OUTPUT_LOG_FILE}
EXIT_STATUS=$[$EXIT_STATUS+$?]
sudo touch ${LOG_FILE}
EXIT_STATUS=$[$EXIT_STATUS+$?]
sudo chmod 666 ${LOG_FILE}
EXIT_STATUS=$[$EXIT_STATUS+$?]
check $EXIT_STATUS

mise_a_jour

script_header

#====Laravel========#
installer_apache2
installer_php
installer_composer
installer_application
configuration_apache2
#===================#

clear
echo -e " "
echo -e " ${CC}Partie optionnel${CE} "
echo -e " "
echo -e " Serveur ssh avec une cle ssh"
echo -e " Adminer"
echo -e " MariaDB comme serveur de base de donnees SQL"
echo -e " "

waitUserInput

#=====optionnels===#
installer_ssh
installer_adminer
installer_mariadb
#==================#

